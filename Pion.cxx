#include "Piece.h"
#include "Echiquier.h"
#include <iostream>
#include "Pion.h"

Pion::Pion(int x, bool white) : Piece(x, (white?2:7), white) { }

bool
Pion::mouvementValide(Echiquier &e, int x, int y)
{
  int caseAllowingTwoMoves = 6;
  int moveDirectionOne = 0;
  int moveDirectionTwo = 0;
  if (m_white)
  {
    caseAllowingTwoMoves = 1;
    moveDirectionOne = 1;
    moveDirectionTwo = 2;
  }
  else
  {
    caseAllowingTwoMoves = 6;
    moveDirectionOne = -1;
     moveDirectionTwo = -2;
  }

  if((m_x == x )&& (m_y+moveDirectionOne == y)){
    if(e.getPiece(x,y) != NULL){
      return false;
    }
  }
  else if((m_x == x )&& (m_y+moveDirectionTwo == y)){
    if(m_y != caseAllowingTwoMoves){
      return false;
    }
    if((e.getPiece(x,y) != NULL)||(e.getPiece(x,m_y+moveDirectionOne) != NULL)){
      return false;
    }
  }
  else if(((m_x+1 == x )&& (m_y+moveDirectionOne == y))||((m_x-1 == x )&& (m_y+moveDirectionOne == y))){
    if(e.getPiece(x,y) == NULL){
      return false;
    }
    else if(e.getPiece(x,y)->isWhite() == isWhite()){
      return false;
    }
  }
  return true;
}

char
Pion::type()
{
  return m_white ? 'P' : 'p';
}
