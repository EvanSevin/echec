/**
 * Programme test de Piece
 *
 * @file testPiece.cxx
 */

// Utile pour l'affichage
#include <iostream>
#include <assert.h>
#include "Piece.h"
#include "Joueur.h"
#include "JoueurBlanc.h"
#include "JoueurNoir.h"
#include "Echiquier.h"
#include "Pion.h"
#include "Roi.h"


// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;


/**
 * Programme principal
 */
int main( int argc, char** argv )
{
  JoueurNoir jn;
  JoueurBlanc jb;

  Echiquier e;
  e.affiche();

  bool piecesOK;
  piecesOK = jn.placerPieces(e);
  assert(piecesOK);
  piecesOK = jb.placerPieces(e);
  assert(piecesOK);

  jn.affiche();
  jb.affiche();


  return 0;
}
