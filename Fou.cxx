#include "Piece.h"
#include "Fou.h"
#include "Echiquier.h"
#include <iostream>

Fou::Fou() { }

Fou::Fou(bool white, bool left) : Piece((left?3:6),(white?1:8),white) { }

bool Fou::mouvementValide(Echiquier &e, int x, int y)
{
  int deltaX;
  int deltaY;
  int srcX = m_x;
  int srcY = m_y;

  if (srcX < x)
    deltaX = 1;
  else
    deltaX = -1;

  if (srcY < y)
    deltaY = 1;
  else
    deltaY = -1;

  srcX += deltaX;
  srcY += deltaY;
  while ((srcX != (x - deltaX)) && (srcY != (y - deltaY)))
  {
    if (e.getPiece(srcX,srcY) != NULL )
    {
      return(false);
    }

    srcX += deltaX;
    srcY += deltaY;
  }

  if(e.getPiece(x,y) != NULL){
    if(e.getPiece(x,y)->isWhite() == isWhite()){
      return false;
    }
  }

  return true;
}

char
Fou::type()
{
  return m_white ? 'F' : 'f';
}
