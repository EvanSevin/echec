#include "Piece.h"
#include "Tour.h"
#include "Echiquier.h"
#include <iostream>

Tour::Tour() { }

Tour::Tour(bool white, bool left) : Piece((left?1:8),(white?1:8),white) { }

bool
Tour::mouvementValide(Echiquier &e, int x, int y)
{
  cout << "mouvementValide de Tour" << endl;
  if((x =! m_x) && (y != m_y)){
    return false;
  }
  else if((x == m_x) && (y == m_y)){
    return false;
  }
  else {
    bool ret = true;
    if(x == m_x){
      int var = m_x;
      while( x != var ){
        if(e.getPiece(var,y) == NULL){
          return false;
        }
        if(x > var){
          var++;
        }
        else{
          var--;
        }
      }
    }
    if(y == m_y){
      int var = m_y;
      while( y != var) {
        if(e.getPiece(var,y) == NULL){
          return false;
        }
        if(y > var){
          var++;
        }
        else{
          var--;
        }
      }
    }
    if(e.getPiece(x,y) != NULL){
      if(e.getPiece(x,y)->isWhite() == isWhite()){
        return false;
      }
    }
  }
  return true;
}

char
Tour::type()
{
  return m_white ? 'T' : 't';
}
